from flask import Flask, render_template, abort, request

app = Flask(__name__)

CAT = """
     ^ ^
   =(   )=
"""

@app.route("/")
def hello():
    return "HELLO!"


@app.route('/<name>')
def checkfile(name):
	path = str(request.__dict__)
	start = path.find("RAW_URI")
	end = path.find("REMOTE_ADDR")
	raw_url = path[start:end]

	if ((raw_url.find("//") or raw_url.find('~') or raw_url.find("..")) != -1):
			return error_403(403)
	else:
		try:
			return render_template(name)
		except:
			abort(404)
	
#----------------ERROR HANDLES-----------------#
@app.errorhandler(404)
def error_404(e):
	#THE FUNK TOWN
	#404 = file forbidden
	#Using the dict to get the raw udi, and using find() to get just the raw url
	#to check if there is a forbidden charater.
	path = str(request.__dict__)
	start = path.find("RAW_URI")
	end = path.find("REMOTE_ADDR")
	raw_url = path[start:end]
	#raw_url = path.find("RAW_URL",0, "REMOTE_ADDR")
	if ((raw_url.find('html') or raw_url.find('css')) != -1):
		if ((raw_url.find("//") or raw_url.find('~') or raw_url.find("..")) != -1):

			return error_403(403)
		
		else:
			#return path
			return render_template('404.html'),404
	else:
		return error_403(403)

@app.errorhandler(403)
def error_403(e):
	return render_template('403.html'),403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
